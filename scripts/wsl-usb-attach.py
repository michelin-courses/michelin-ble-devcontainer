#!/usr/bin/python3
import subprocess
import re

# Command that list all the device connected to the host computer
commande = "usbipd list"

# Run the command and save the result
result = subprocess.run(commande, shell=True, capture_output=True, text=True)

lines = result.stdout.splitlines()

not_attached_device_count = [line for line in lines if "Shared" in line]

print(f"WSL usb attach try to process on {len(not_attached_device_count)} devices that are not yet attached: \n")

for line in lines:
    if "Shared" in line:

        parts = [re.split(r'\s{2,}', line.strip())]
        line_content = [item for sublist in parts for item in sublist]

        print(f"Device: {line_content[0]} {line_content[2]} is attached to WSL2")
        try:
            output = subprocess.check_output(
                "usbipd attach -w --busid={}".format(line_content[0]),
                universal_newlines=True,
                shell=True
            )
        except subprocess.CalledProcessError:
            pass

        print("\n")
